# ServiceInfo

ServiceInfo is a specification for generic metadata about a web service. The specification
provides a range of standard properties for service owners to fill to describe their
service.

ServiceInfo documents are [JSON-LD](https://www.w3.org/TR/json-ld/) documents. Thus additional 
properties can be used via JSON-LD extensions by providing a context that describes the 
properties.

ServiceInfo documents are discovered via host-meta document lookups and from other
JSON-LD documents containing the ServiceInfo context. 

## Specification

Please see the [specification](SPECIFICATION.md).

## Support

Implemented in the following platforms:

* TBD

Library support is available for the following:

* TBD

Have a server or library you have added support to? Send a PR!

## License

All content in this repository is under [CC0](http://creativecommons.org/publicdomain/zero/1.0/) 
unless otherwise noted.

## Contributing

Please open issues and pull requests if you want to suggest a change. If you open a pull 
request you agree for your work to be released under CC0.

We have a [forum for discussion](https://talk.feneas.org/t/serviceinfo-specification-for-service-metadata/99).

## History

ServiceInfo is a fork of [NodeInfo2](https://git.feneas.org/jaywink/nodeinfo2) which itself 
is a fork of [NodeInfo](https://nodeinfo.diaspora.software/) which was the successor of 
`statistics.json` in Diaspora.
