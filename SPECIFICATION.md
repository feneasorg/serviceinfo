# ServiceInfo specification 1.0

## Description

ServiceInfo is a specification for generic metadata about a web service. The specification
provides a range of standard properties for service owners to fill to describe their
service.

ServiceInfo documents are [JSON-LD](https://www.w3.org/TR/json-ld/) documents. Thus additional 
properties can be used via JSON-LD extensions by providing a context that describes the 
properties.

ServiceInfo documents are discovered via host-meta document lookups and from other
JSON-LD documents containing the ServiceInfo context. 

## Status

This document is in early draft status.

## Definitions

The term "service" in this document refers to a service providing metadata about itself.

The term "client" in this document refers to software wishing to retrieve metadata about 
a service.

The term "schema" refers to a schema definition provided in the schemas subdirectory.

## Document discovery

ServiceInfo documents are discovered as follows:

* A `host-meta` lookup is made as per IETF [RFC6415](https://wiki.tools.ietf.org/html/rfc6415).
  Clients MUST support the XRD representation but SHOULD also support the JRD representation. 

* From the `host-meta` the client looks up a `Link` element with `rel` of 
  `https://feneas.org/ns/serviceinfo`.
  
* If there are several, a client MAY choose which `Link` element they are interested by
  looking at the `properties` of the ServiceInfo link elements in the host-meta document. 
  
* The client makes a GET to the `href` of the found link element. The request SHOULD be made
  with an `Accept: application/ld+json` header.
  
Services MUST provide a `host-meta` as above for discovery by clients. Multiple ServiceInfo
documents in the same host-meta SHOULD be attached a `software.name` property, to allow
clients to choose the one they are interested in.

### Host-meta examples

**XRD**

`GET /.well-known/host-meta`

Response with content type `application/xrd+xml`:

```xml
<?xml version='1.0' encoding='UTF-8'?>
<XRD xmlns='http://docs.oasis-open.org/ns/xri/xrd-1.0'
     xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
>
    <Link 
        rel='https://feneas.org/ns/serviceinfo' 
        type='application/ld+json' 
        href='https://domain.tld/_serviceinfo'
    >
        <Property type='https://feneas.org/ns/serviceinfo#software.name'>
            Acme Service
        </Property>
    </Link>
</XRD>
```

**JRD**

`GET /.well-known/host-meta.json`

Response with content type `application/json`:

```json 
{
  "links": [
    {
      "rel": "https://feneas.org/ns/serviceinfo",
      "type": "application/ld+json",
      "href": "https://domain.tld/_serviceinfo",
      "properties": {
        "https://feneas.org/ns/serviceinfo#software.name": "Acme Service"
      }
    }
  ]
}
```

## Document location in ActivityStreams2 objects

Services implementing ActivityStreams2 objects MAY have an additional context of 
`https://feneas.org/ns/serviceinfo` indicating the presence of an URL to a ServiceInfo 
document of the service the object is hosted at. Clients encountering objects with the 
context MAY fetch the ServiceInfo documents.

The URL to the document should be indicated as the property `serviceInfoUrl` with the correct
context. For example:

```json
{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://feneas.org/ns/serviceinfo"
  ],
  ...
  "serviceInfoUrl": "https://domain.tld/_serviceinfo",
  ...
}
```

## Schema

Services MUST produce a ServiceInfo document following the 
[JSON schema](/schemas/1.0/schema.json).

## Authors

Main authors:

* Jason Robinson / Federated Networks Association

Feedback and review provided by the following people:

* bob
* grin
* Hank Grabowski
* Marius Orcsik
* nolan

ServiceInfo is based on [NodeInfo](http://nodeinfo.diaspora.software/) and 
[NodeInfo2](https://git.feneas.org/jaywink/nodeinfo2).

## License

CC0
